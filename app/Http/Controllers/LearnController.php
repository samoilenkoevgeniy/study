<?php

namespace App\Http\Controllers;

use App\Library;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redis;

class LearnController extends Controller
{
    public function learn()
    {
        ini_set('max_execution_time', 360);
        $strings = Library::all();
        foreach($strings as $string) {
            $words = explode(" ", $string->sting);
            foreach($words as $word) {
                Redis::set($word, $string->language);
            }
        }
    }
}
