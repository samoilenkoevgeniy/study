<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Redis;

class WorkController extends Controller
{
    public function work()
    {
        ini_set('max_execution_time', 360);
        $handle = fopen($_SERVER['DOCUMENT_ROOT']."/test.txt", "r");
        $counter = 0;
        $result = [];
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $words = explode(" ", $buffer);
                $arr = [];
                foreach($words as $word) {
                    $value = Redis::get($word);
                    if(!is_null($value)) {
                        $arr[] = $value;
                    }
                }
                if(empty($arr)) {
                    $result[] = "";
                    continue;
                }
                $array = array_count_values($arr);
                $key_max = array_keys($array, max($array))[0];
                $result[] = $key_max;
                $counter++;

            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
        dd($result);
    }
}
