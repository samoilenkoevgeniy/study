<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    protected $fillable = [
        "language", "sting"
    ];

    public static function getLibrary()
    {
        ini_set('max_execution_time', 360);
        $handle = fopen($_SERVER['DOCUMENT_ROOT']."/train.txt", "r");
        $counter = 0;
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $pieces = explode("\t", $buffer);
                $data = [
                    "language" => $pieces[0],
                    "sting" => $pieces[1],
                ];
                Library::create($data);
                $counter++;
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
    }
}
